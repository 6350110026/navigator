import 'package:flutter/material.dart';

import '../../../constants.dart';

class LoginScreenTopImage extends StatelessWidget {
  const LoginScreenTopImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "LOGIN",
          style: TextStyle(
            fontSize: 38,fontWeight: FontWeight.bold,color: Colors.indigo,
          ),
        ),

        Padding(padding: EdgeInsets.all(4)),

        Text(
          "Please login before use.",
          style: TextStyle(
            fontSize: 18,
          ),
        ),
        SizedBox(height: defaultPadding * 2),
        Row(
          children: [
            const Spacer(),
            Expanded(
              flex: 8,
              child: Icon(Icons.health_and_safety_outlined,size: 200,color: Colors.lightBlue,),
            ),
            const Spacer(),
          ],
        ),
        SizedBox(height: defaultPadding * 2),
      ],
    );
  }
}