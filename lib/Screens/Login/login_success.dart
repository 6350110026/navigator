import 'package:flutter/material.dart';
import 'package:nav_app/Screens/Login/components/backbtn.dart';
import 'package:nav_app/Screens/Login/components/login_success_image.dart';
import 'package:nav_app/Screens/Login/login_screen.dart';
import 'package:nav_app/responsive.dart';

import '../../../components/background.dart';
import '../Welcome/components/login_signup_btn.dart';

class LoginSuccess extends StatelessWidget {
  const LoginSuccess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Background(
      child: SingleChildScrollView(
        child: SafeArea(
          child: Responsive(
            desktop: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Expanded(
                  child: LoginSucessPage(),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(
                        width: 450,
                        child: LoginAndSignupBtn(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            mobile: const MobileLoginSuccessScreen(),
          ),
        ),
      ),
    );
  }
}

class MobileLoginSuccessScreen extends StatelessWidget {
  const MobileLoginSuccessScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const LoginSucessPage(),
        Row(
          children: const [
            Spacer(),
            Expanded(
              flex: 8,
              child: BackBtn(),
            ),
            Spacer(),
          ],
        ),
      ],
    );
  }
}

