import 'package:flutter/material.dart';

const kPrimaryColor = Colors.blue;
const kPrimaryLightColor = Colors.black12;

const double defaultPadding = 16.0;
